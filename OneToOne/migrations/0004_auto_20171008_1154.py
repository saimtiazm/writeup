# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('OneToOne', '0003_auto_20171008_1148'),
    ]

    operations = [
        migrations.RenameField(
            model_name='univstudent',
            old_name='student',
            new_name='user',
        ),
    ]
