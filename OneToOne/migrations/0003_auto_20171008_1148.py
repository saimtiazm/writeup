# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('OneToOne', '0002_remove_univstudent_sid'),
    ]

    operations = [
        migrations.RenameField(
            model_name='univstudent',
            old_name='subject',
            new_name='subject_major',
        ),
    ]
